import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';
import WelcomePage from './components/WelcomePage';
import LoginPage from './components/LoginPage';
import StartTeam from './components/StartTeam';
import ProjectPage from './components/ProjectPage';
import NotFound from './components/NotFound';

class App extends Component {
    render() {
        return (
            <Router>
                <div className="App">
                    <Switch>
                        <Route exact path="/" component={WelcomePage}/>
                        <Route exact path="/startteam" component={StartTeam}/>
                        <Route exact path="/login" component={LoginPage}/>
                        <Route exact path="/project" component={ProjectPage}/>
                        <Route path="*" component={NotFound}/>
                    </Switch>
                </div>
            </Router>
        );

    }
}
export default App;
