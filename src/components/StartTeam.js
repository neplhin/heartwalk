import React, {Component} from 'react';
import Header from './Header';
import {connect} from "react-redux";
import {Button, ControlLabel, FormControl, FormGroup} from "react-bootstrap";


class StartTeam extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }


    async componentDidMount() {
        console.log()

        this.setState({user: this.props.user});
    }

    render() {
        const {user, isAuthenticated} = this.props;
        console.log(this.state);
        return (
            <div style={{backgroundColor: "#eee", color: "#000"}}>
                <Header/>
                <div className="container">
                    <div className="simpleBaner"></div>
                    <div className="createTeamFormBlock">
                        <h1>Start a Team</h1>
                        <p>Please select your company from the drop-down box below and create your team.</p>
                        <form className="startteam-form">
                            <FormGroup bsSize="large">
                                <ControlLabel><span>Team Company:</span></ControlLabel>
                                <FormControl componentClass="select">
                                    <option value="select">Choose an existing company</option>
                                    <option value="select">Company 1</option>
                                    <option value="select">Company 2</option>
                                    <option value="select">Company 3</option>
                                    <option value="other">...</option>
                                </FormControl>
                            </FormGroup>

                            <FormGroup  bsSize="large">
                                <ControlLabel><span>Team Name:</span></ControlLabel>
                                <FormControl
                                    type="text"
                                    value={this.state.value}
                                    placeholder=""
                                    onChange={this.handleChange}
                                />
                            </FormGroup>

                            <FormGroup bsSize="large">
                                <ControlLabel><span>Team Fundraising Goal:</span><span style={{fontSize: ".8em", color: "#767676", display:"block"}}><br />Suggested Team Goal: $2,500.00</span></ControlLabel>
                                <FormControl
                                    type="text"
                                    value={this.state.value}
                                    placeholder=""
                                    onChange={this.handleChange}
                                />
                            </FormGroup>

                            <Button className="border-radius-unset" bsStyle="danger" bsSize="large" type="submit">Next Step</Button>
                        </form>
                    </div>
                </div>

            </div>

        )
    }
}

const mapStateToProps = state => ({
    ...state
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(StartTeam);